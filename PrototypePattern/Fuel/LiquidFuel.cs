﻿namespace PrototypePattern.Fuel
{
    public enum LiquidFuel
    {
        Kerosene,
        Diesel,
        Benzine
    }
}
