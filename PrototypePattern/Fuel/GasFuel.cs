﻿namespace PrototypePattern.Fuel
{
    public enum GasFuel
    {
        Natural,
        Methane,
        Propane,
        Butane,
        Bio,
        Hydrogen,
        Mixture
    }
}
