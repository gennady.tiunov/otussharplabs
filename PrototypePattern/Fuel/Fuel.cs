﻿namespace PrototypePattern.Fuel
{
    public enum Fuel
    {
        Liquid,
        Gas
    }
}
