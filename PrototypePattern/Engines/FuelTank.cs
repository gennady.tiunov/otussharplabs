﻿using PrototypePattern.Prototype;

namespace PrototypePattern.Engines
{
    public class FuelTank : ICloneable<FuelTank>, ICloneable
    {
        public int Capacity { get; }

        public int FuelLevel { get; set; }

        public FuelTank(
            int capacity,
            int fuelLevel)
        {
            Capacity = capacity;
            FuelLevel = fuelLevel;
        }

        protected FuelTank(FuelTank obj)
        {
            Capacity = obj.Capacity;
            FuelLevel = obj.FuelLevel;
        }

        public FuelTank Clone()
        {
            return new FuelTank(this);
        }

        object ICloneable.Clone()
        {
            return Clone();
        }
    }
}