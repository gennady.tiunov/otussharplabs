﻿using PrototypePattern.Prototype;

namespace PrototypePattern.Engines
{
    public abstract class Engine : ICloneable<Engine>, ICloneable
    {
        public float Power { get; }

        private Engine() { }

        protected Engine(float power) : this()
        {
            Power = power;
        }

        protected Engine(Engine obj) : this()
        {
            Power = obj.Power;
        }

        public abstract Engine Clone();

        object ICloneable.Clone()
        {
            return Clone();
        }
    }
}
