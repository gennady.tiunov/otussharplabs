﻿using PrototypePattern.Fuel;
using PrototypePattern.Prototype;

namespace PrototypePattern.Engines
{
    public class LiquidFuelEngine : InternalCombustionEngine, ICloneable<LiquidFuelEngine>, ICloneable
    {
        public LiquidFuel LiquidFuel { get; }

        public LiquidFuelEngine(
            float power,
            LiquidFuel liquidFuel) : base(power, PrototypePattern.Fuel.Fuel.Liquid)
        {
            LiquidFuel = liquidFuel;
        }

        protected LiquidFuelEngine(LiquidFuelEngine obj) :
            base(obj)
        {
            LiquidFuel = obj.LiquidFuel;
        }

        public override LiquidFuelEngine Clone()
        {
            return new LiquidFuelEngine(this);
        }

        object ICloneable.Clone()
        {
            return Clone();
        }
    }
}
