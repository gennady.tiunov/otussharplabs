﻿using PrototypePattern.Prototype;

namespace PrototypePattern.Engines
{
    public class InternalCombustionEngine : Engine, ICloneable<InternalCombustionEngine>, ICloneable
    {
        public Fuel.Fuel Fuel { get; }

        public InternalCombustionEngine(
            float power,
            Fuel.Fuel fuel) : base(power)
        {
            Fuel = fuel;
        }

        protected InternalCombustionEngine(InternalCombustionEngine obj) : base(obj)
        {
            Fuel = obj.Fuel;
        }

        public override InternalCombustionEngine Clone()
        {
            return new InternalCombustionEngine(this);
        }

        object ICloneable.Clone()
        {
            return Clone();
        }
    }
}
