﻿using PrototypePattern.Fuel;
using PrototypePattern.Prototype;

namespace PrototypePattern.Engines
{
    public class GasFuelEngine : InternalCombustionEngine, ICloneable<GasFuelEngine>, ICloneable
    {
        public GasFuel GasFuel { get; }

        public GasFuelEngine(
            float power,
            GasFuel gasFuel) : base(power, PrototypePattern.Fuel.Fuel.Gas)
        {
            GasFuel = gasFuel;
        }

        protected GasFuelEngine(GasFuelEngine obj) : base(obj)
        {
            GasFuel = obj.GasFuel;
        }

        public override GasFuelEngine Clone()
        {
            return new GasFuelEngine(this);
        }

        object ICloneable.Clone()
        {
            return Clone();
        }
    }
}
