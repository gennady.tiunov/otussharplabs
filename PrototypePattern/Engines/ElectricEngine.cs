﻿using PrototypePattern.Prototype;

namespace PrototypePattern.Engines
{
    public class ElectricEngine : Engine, ICloneable<ElectricEngine>, ICloneable
    {
        public ElectricEngine(float power) : base(power)
        {
        }

        protected ElectricEngine(ElectricEngine obj) : base(obj)
        {
        }

        public override ElectricEngine Clone()
        {
            return new ElectricEngine(this);
        }

        object ICloneable.Clone()
        {
            return Clone();
        }
    }
}