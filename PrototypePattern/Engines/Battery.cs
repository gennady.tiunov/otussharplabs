﻿using PrototypePattern.Prototype;

namespace PrototypePattern.Engines
{
    public class Battery : ICloneable<Battery>, ICloneable
    {
        public int Capacity { get; }

        public int ChargeLevel { get; set; }

        public Battery(
            int capacity,
            int chargeLevel)
        {
            Capacity = capacity;
            ChargeLevel = chargeLevel;
        }

        private Battery(Battery obj)
        {
            Capacity = obj.Capacity;
            ChargeLevel = obj.ChargeLevel;
        }

        public Battery Clone()
        {
            return new Battery(this);
        }

        object ICloneable.Clone()
        {
            return Clone();
        }
    }
}