﻿using PrototypePattern.Prototype;

namespace PrototypePattern.Vehicles
{
    public abstract class Vehicle : ICloneable<Vehicle>, ICloneable
    {
        public int Mileage { get; set; }

        private Vehicle()  { }

        protected Vehicle(int mileage) : this()
        {
            Mileage = mileage;
        }

        protected Vehicle(Vehicle obj) : this()
        {
            Mileage = obj.Mileage;
        }

        public abstract void Drive();
        
        public abstract Vehicle Clone();

        object ICloneable.Clone()
        {
            return Clone();
        }
    }
}
