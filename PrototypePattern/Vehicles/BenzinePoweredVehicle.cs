﻿using System.Text.Json;
using System.Text.Json.Serialization;
using PrototypePattern.Engines;
using PrototypePattern.Prototype;

namespace PrototypePattern.Vehicles
{
    public class BenzinePoweredVehicle : Vehicle, ICloneable<BenzinePoweredVehicle>, ICloneable
    {
        public LiquidFuelEngine Engine { get; }

        public FuelTank Tank { get; }

        public BenzinePoweredVehicle(
            LiquidFuelEngine liquidFuelEngine,
            FuelTank tank,
            int mileage) : base(mileage)
        {
            Engine = liquidFuelEngine;
            Tank = tank;
        }

        private BenzinePoweredVehicle(BenzinePoweredVehicle obj) : base(obj)
        {
            Engine = obj.Engine.Clone();
            Tank = obj.Tank.Clone();
        }

        public override void Drive()
        {
            Tank.FuelLevel -= 1;
            Mileage += 1;
        }

        public override BenzinePoweredVehicle Clone()
        {
            return new BenzinePoweredVehicle(this);
        }
        
        object ICloneable.Clone()
        {
            return Clone();
        }

        public override string ToString()
        {
            return $"{nameof(BenzinePoweredVehicle)}: {JsonSerializer.Serialize(this, new JsonSerializerOptions { Converters = { new JsonStringEnumConverter() } })}";
        }
    }
}