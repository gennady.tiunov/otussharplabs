﻿using System.Text.Json;
using System.Text.Json.Serialization;
using PrototypePattern.Engines;
using PrototypePattern.Prototype;

namespace PrototypePattern.Vehicles
{
    public class GasPoweredVehicle : Vehicle, ICloneable<GasPoweredVehicle>, ICloneable
    {
        public GasFuelEngine Engine { get; }

        public FuelTank Tank { get; }

        public GasPoweredVehicle(
            GasFuelEngine gasFuelEngine,
            FuelTank tank,
            int mileage) :  base(mileage)
        {
            Engine = gasFuelEngine;
            Tank = tank;
        }

        private GasPoweredVehicle(GasPoweredVehicle obj) : base(obj)
        {
            Engine = obj.Engine.Clone();
            Tank = obj.Tank.Clone();
        }

        public override void Drive()
        {
            Tank.FuelLevel -= 1;
            Mileage += 1;
        }

        public override GasPoweredVehicle Clone()
        {
            return new GasPoweredVehicle(this);
        }

        object ICloneable.Clone()
        {
            return Clone();
        }

        public override string ToString()
        {
            return $"{nameof(GasPoweredVehicle)}: {JsonSerializer.Serialize(this, new JsonSerializerOptions { Converters = { new JsonStringEnumConverter() } })}";
        }
    }
}