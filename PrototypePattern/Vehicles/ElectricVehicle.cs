﻿using System.Text.Json;
using System.Text.Json.Serialization;
using PrototypePattern.Engines;
using PrototypePattern.Prototype;

namespace PrototypePattern.Vehicles
{
    public class ElectricVehicle : Vehicle, ICloneable<ElectricVehicle>, ICloneable
    {
        public ElectricEngine Engine { get; }

        public Battery Battery { get; }

        public ElectricVehicle(
            ElectricEngine electricEngine,
            Battery battery,
            int mileage) : base(mileage)
        {
            Engine = electricEngine;
            Battery = battery;
        }

        private ElectricVehicle(ElectricVehicle obj) : base(obj)
        {
            Engine = obj.Engine.Clone();
            Battery = obj.Battery.Clone();
        }

        public override void Drive()
        {
            Battery.ChargeLevel -= 1;
            Mileage += 1;
        }

        public override ElectricVehicle Clone()
        {
            return new ElectricVehicle(this);
        }

        object ICloneable.Clone()
        {
            return Clone();
        }

        public override string ToString()
        {
            return $"{nameof(ElectricVehicle)}: {JsonSerializer.Serialize(this, new JsonSerializerOptions { Converters = { new JsonStringEnumConverter() } })}";
        }
    }
}