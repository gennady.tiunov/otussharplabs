﻿using PrototypePattern.Engines;
using PrototypePattern.Fuel;
using PrototypePattern.Vehicles;

Console.Clear();
Console.WriteLine();

var originalElectricVehicle = new ElectricVehicle(
    new ElectricEngine(200),
    new Battery(1000, 100),
    1);

var clonedElectricVehicle = ((ICloneable)originalElectricVehicle).Clone();

originalElectricVehicle.Drive();

Console.WriteLine($"Original electric vehicle: {originalElectricVehicle}");
Console.WriteLine($"Cloned electric vehicle: {clonedElectricVehicle }");

Console.WriteLine();

var originalGasPoweredVehicle = new GasPoweredVehicle(
    new GasFuelEngine(150, GasFuel.Methane),
    new FuelTank(50, 50),
    2);

var clonedGasPoweredVehicle = originalGasPoweredVehicle.Clone();

originalGasPoweredVehicle.Drive();
originalGasPoweredVehicle.Drive();

Console.WriteLine($"Original gas powered vehicle: {originalGasPoweredVehicle}");
Console.WriteLine($"Cloned gas powered vehicle: {clonedGasPoweredVehicle}");

Console.WriteLine();

var originalBenzinePoweredVehicle = new BenzinePoweredVehicle(
    new LiquidFuelEngine(300, LiquidFuel.Benzine),
    new FuelTank(75, 75),
    5);

var clonedBenzinePoweredVehicle = originalBenzinePoweredVehicle.Clone();

originalBenzinePoweredVehicle.Drive();
originalBenzinePoweredVehicle.Drive();
originalBenzinePoweredVehicle.Drive();

Console.WriteLine($"Original gas powered vehicle: {originalBenzinePoweredVehicle}");
Console.WriteLine($"Cloned gas powered vehicle: {clonedBenzinePoweredVehicle}");

Console.WriteLine();
Console.WriteLine("Press any key");
Console.ReadKey();