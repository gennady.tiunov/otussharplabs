﻿namespace GuessNumberGame
{
    public class SimpleGamePlayer : IGamePlayer
    {
        public void PlayGame(IGame game)
        {
            if (game == null) 
            {
                throw new ArgumentNullException(nameof(game));
            }

            game.Play();
        }
    }
}
