﻿namespace GuessNumberGame
{
    public class InvalidNumberFormatException : InvalidCastException
    {
        internal InvalidNumberFormatException(string numberCandidate)
            : base($"'{numberCandidate}' is not a valid number!")
        { }
    }
}
