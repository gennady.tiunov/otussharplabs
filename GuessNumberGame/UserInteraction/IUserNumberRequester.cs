﻿namespace GuessNumberGame
{
    public interface IUserNumberRequester
    {
        byte RequestNumber();
    }
}
