﻿namespace GuessNumberGame
{
    public class ConsoleNumberRequester : IUserNumberRequester
    {
        public byte RequestNumber()
        {
            var input = Console.ReadLine();

            if (!byte.TryParse(input, out var number))
            {
                throw new InvalidNumberFormatException(input);
            }

            return number;
        }
    }
}