﻿namespace GuessNumberGame
{
    public class ConsoleUserNotifier : IUserNotifier
    {
        public void NotifyUser(string message)
        {
            Console.WriteLine(message);
        }
    }
}