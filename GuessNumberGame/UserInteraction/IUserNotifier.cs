﻿namespace GuessNumberGame
{
    public interface IUserNotifier
    {
        void NotifyUser(string message);
    }
}
