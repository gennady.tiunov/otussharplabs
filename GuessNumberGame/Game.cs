﻿namespace GuessNumberGame
{
    public class Game : IGame
    {
        private readonly GameConfiguration _gameConfiguration;

        private readonly IUserNumberRequester _userNumberRequester;
        private readonly IUserNotifier _userNotifier;

        public Game(
            GameConfigurationBuilderBase gameConfigurationBuilder,
            IUserNumberRequester userNumberRequester,
            IUserNotifier userNotifier)
        {
            _gameConfiguration = gameConfigurationBuilder.Build();
            _userNumberRequester = userNumberRequester;
            _userNotifier = userNotifier;
        }

        public void Play()
        {
            byte currentAttempt = 1;

            byte enteredNumber = 0;
            do
            {
                byte attemptsLeft = (byte)(_gameConfiguration.NumberOfAttempts - (currentAttempt - 1));

                _userNotifier.NotifyUser(BuildGenericInputMessage(attemptsLeft));

                try
                {
                    enteredNumber = _userNumberRequester.RequestNumber();
                    if (enteredNumber == _gameConfiguration.NumberToGuess)
                    {
                        _userNotifier.NotifyUser(BuildWinnerMessage());
                    }
                    else
                    {
                        if (enteredNumber < _gameConfiguration.NumberToGuess)
                        {
                            _userNotifier.NotifyUser(BuildGreaterMessage(enteredNumber));
                        }
                        else if (enteredNumber > _gameConfiguration.NumberToGuess)
                        {
                            _userNotifier.NotifyUser(BuildLessMessage(enteredNumber));
                        }

                        if (_gameConfiguration.NumberOfAttempts == currentAttempt)
                        {
                            _userNotifier.NotifyUser(BuildLoserMessage());
                        }
                    }
                }
                catch (InvalidNumberFormatException exception)
                {
                    _userNotifier.NotifyUser(BuildInvalidInputMessage(exception.Message));
                    currentAttempt--;
                }
            }
            while (enteredNumber != _gameConfiguration.NumberToGuess && _gameConfiguration.NumberOfAttempts > currentAttempt++);
        }

        private string BuildGenericInputMessage(byte attemptsLeft) => $"Try to guess the number from '{_gameConfiguration.Range.LowerThreshold}' to '{_gameConfiguration.Range.UpperThreshold}' (you have {attemptsLeft} attempts left): ";

        private string BuildWinnerMessage() => $"Congratulations, you won!";

        private string BuildLoserMessage() => $"Sorry, that was your last attempt, you lose!";

        private string BuildLessMessage(byte number) => $"The number you need to guess is less than {number}.";

        private string BuildGreaterMessage(byte number) => $"The number you need to guess is greater than {number}.";

        private string BuildInvalidInputMessage(string exceptionMessage) => exceptionMessage;
    }
}
