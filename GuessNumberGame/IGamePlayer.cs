﻿namespace GuessNumberGame
{
    public interface IGamePlayer
    {
        void PlayGame(IGame game);
    }
}