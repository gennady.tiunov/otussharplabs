﻿namespace GuessNumberGame
{
    public class ByteNumberRange
    {
        public byte LowerThreshold { get; set; }

        public byte UpperThreshold { get; set; }
    }
}
