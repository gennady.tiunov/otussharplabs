﻿namespace GuessNumberGame
{
    public class AdvancedGameConfigurationBuilder : GameConfigurationBuilderBase
    {
        private readonly IByteNumberGenerator _byteNumberGenerator;

        public AdvancedGameConfigurationBuilder(IByteNumberGenerator byteNumberGenerator)
        {
            _byteNumberGenerator = byteNumberGenerator;
        }

        public override GameConfiguration Build()
        {
            var gameConfiguration = base.Build();

            var numberToGuess = _byteNumberGenerator.GenerateNumber(gameConfiguration.Range);

            gameConfiguration.NumberToGuess = numberToGuess;

            return gameConfiguration;
        }
    }
}
