﻿using GuessNumberGame;

public class ByteNumberGenerator : IByteNumberGenerator
{
    public byte GenerateNumber(ByteNumberRange range)
    {
        if (range == null)
        {
            throw new ArgumentNullException(nameof(range));
        }

        var rnd = new Random();

        return (byte) rnd.Next(range.LowerThreshold, range.UpperThreshold);
    }
}
