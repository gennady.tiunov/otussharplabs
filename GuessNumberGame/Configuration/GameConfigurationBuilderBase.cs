﻿using Microsoft.Extensions.Configuration;

namespace GuessNumberGame
{
    public abstract class GameConfigurationBuilderBase
    {
        public virtual GameConfiguration Build()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: false);

            IConfiguration appConfiguration = builder.Build();

            var gameConfiguration = appConfiguration.GetSection("GameConfiguration").Get<GameConfiguration>();
            if (gameConfiguration.Range.LowerThreshold >= gameConfiguration.Range.UpperThreshold)
            {
                throw new InvalidConfigurationException("Lower number range threshold must be less than upper threshold");
            }

            return gameConfiguration;
        }
    }
}