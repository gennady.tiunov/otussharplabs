﻿namespace GuessNumberGame
{
    public class GameConfiguration
    {
        public byte NumberOfAttempts { get; set; }

        public ByteNumberRange Range { get; set;}

        public byte NumberToGuess { get; set; }
    }
}
