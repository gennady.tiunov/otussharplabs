﻿namespace GuessNumberGame
{
    public interface IByteNumberGenerator
    {
        byte GenerateNumber(ByteNumberRange range);
    }
}
