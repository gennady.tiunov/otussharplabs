﻿namespace GuessNumberGame
{
    public class InvalidConfigurationException : InvalidOperationException
    {
        internal InvalidConfigurationException(string message)
            : base(message)
        { }
    }
}
