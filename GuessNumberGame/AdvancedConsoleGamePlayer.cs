﻿namespace GuessNumberGame
{
    public class AdvancedConsoleGamePlayer : IGamePlayer
    {
        public void PlayGame(IGame game)
        {
            if (game == null)
            {
                throw new ArgumentNullException(nameof(game));
            }

            Console.Clear();

            Console.WriteLine("Hi!");
            Console.WriteLine();

            game.Play();

            Console.WriteLine();
            Console.WriteLine("See you!");
        }
    }
}
