﻿using GuessNumberGame;

var game = new Game(
    new AdvancedGameConfigurationBuilder(new ByteNumberGenerator()),
    new ConsoleNumberRequester(),
    new ConsoleUserNotifier());

new AdvancedConsoleGamePlayer().PlayGame(game);
