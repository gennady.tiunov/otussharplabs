﻿namespace GuessNumberGame
{
    public interface IGame
    {
        void Play();
    }
}
