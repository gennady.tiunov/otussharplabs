﻿using Parallelism;
using Parallelism.Summators;

var smallSizeArray = new ArrayGenerator(1_000_000).Generate();
CalculationRunner.Run(new SequentialArraySummator(smallSizeArray));
CalculationRunner.Run(new ThreadArraySummator(smallSizeArray));
CalculationRunner.Run(new TaskArraySummator(smallSizeArray));
CalculationRunner.Run(new TplArraySummator(smallSizeArray));

Console.WriteLine("****************************************************************************************************");

var middleSizeArray = new ArrayGenerator(100_000_000).Generate();
CalculationRunner.Run(new SequentialArraySummator(middleSizeArray));
CalculationRunner.Run(new ThreadArraySummator(middleSizeArray));
CalculationRunner.Run(new TaskArraySummator(middleSizeArray));
CalculationRunner.Run(new TplArraySummator(middleSizeArray));

Console.WriteLine("****************************************************************************************************");

var largeSizeArray = new ArrayGenerator(1_000_000_000).Generate();
CalculationRunner.Run(new SequentialArraySummator(largeSizeArray));
CalculationRunner.Run(new ThreadArraySummator(largeSizeArray));
CalculationRunner.Run(new TaskArraySummator(largeSizeArray));
CalculationRunner.Run(new TplArraySummator(largeSizeArray));

Console.WriteLine("Press any key");
Console.ReadKey();
