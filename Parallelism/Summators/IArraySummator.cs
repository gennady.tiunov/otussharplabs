﻿namespace Parallelism.Summators
{
    internal interface IArraySummator
    {
        int ArrayLength { get; }

        ulong SumArray();
    }
}
