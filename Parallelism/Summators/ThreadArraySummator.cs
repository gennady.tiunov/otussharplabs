﻿namespace Parallelism.Summators
{
    internal class ThreadArraySummator : IArraySummator
    {
        private const int ThreadCount = 7;

        private readonly int _chunkSize;
        private readonly int _lastChunkExcess;

        private readonly byte[] _array;

        internal ThreadArraySummator(byte[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentException("Array is undefined or empty");
            }

            _array = array;

            _chunkSize = array.Length / ThreadCount;
            _lastChunkExcess = array.Length % ThreadCount;
        }

        public int ArrayLength => _array.Length;

        private class IndexThreadParams
        {
            public int StartIndex { get; }

            public int FinishIndex { get; }

            public IndexThreadParams(int startIndex, int finishIndex)
            {
                StartIndex = startIndex;
                FinishIndex = finishIndex;
            }
        }

        public ulong SumArray()
        {
            ulong totalSum = 0;

            void SumSubArray(object? indexParams)
            {
                if (indexParams is not IndexThreadParams indices)
                {
                    throw new ArgumentException($"{nameof(indexParams)} parameter is not of type '{nameof(IndexThreadParams)}'");
                }

                ulong subArraySum = 0;

                var (startIndex, finishIndex) = (indices.StartIndex, indices.FinishIndex);

                for (var i = startIndex; i <= finishIndex; i++)
                {
                    subArraySum += _array[i];
                }

                Interlocked.Add(ref totalSum, subArraySum);
            }

            var threads = new Thread[ThreadCount];

            for (var i = 0; i < ThreadCount; i++)
            {
                var startIndex = _chunkSize * i;
                var finishIndex = startIndex + (_chunkSize - 1);

                if (i == ThreadCount - 1)
                {
                    finishIndex += _lastChunkExcess;
                }

                threads[i] = new Thread(SumSubArray);
                threads[i].Start(new IndexThreadParams(startIndex, finishIndex));
            }

            foreach (var thread in threads)
            {
                thread.Join();
            }
            
            return totalSum;
        }
    }
}