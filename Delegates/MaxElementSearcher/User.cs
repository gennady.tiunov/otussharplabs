﻿namespace MaxElementSearcher
{
    public class User
    {
        public Guid Id { get; set; }

        public string? Name { get; set; }

        public override string ToString()
        {
            return $"{Name} (Id = '{Id}', Hash Code = '{GetHashCode()}')";
        }
        public override int GetHashCode()
        {
            return Math.Abs(Id.GetHashCode());
        }
    }
}