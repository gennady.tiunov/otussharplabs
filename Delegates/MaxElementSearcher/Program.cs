﻿using MaxElementSearcher;

static float GenericToFloat<T>(T element) where T : class
{
    return element.GetHashCode();
}

Console.Clear();

var users = new[]
{
    new User { Id = Guid.NewGuid(), Name = "Ivanov Ivan"},
    new User { Id = Guid.NewGuid(), Name = "Petrov Petr" },
    new User { Id = Guid.NewGuid(), Name = "Sidorov Sidor" }
};

Console.WriteLine("Users:");
foreach (var user in users)
{
    Console.WriteLine(user);
}

Console.WriteLine();
Console.WriteLine($"Max User: {users.GetMax(GenericToFloat)}");

Console.WriteLine();
Console.Write("Press any key...");
Console.ReadKey();