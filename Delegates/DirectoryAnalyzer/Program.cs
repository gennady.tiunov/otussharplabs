﻿using DirectoryAnalyzer;

static void CommonFileFoundHandler(object sender, FileArgs e)
{
    Console.WriteLine($"Another file found - '{e.FileName}'.");
}

static void InterruptingFileFoundHandler(object sender, FileArgs e)
{
    Console.WriteLine($"A file found - '{e.FileName}', it's enough.");
    ((DirectoryAnalyzer.DirectoryAnalyzer)sender).Stop();
}

Console.Clear();

var directoryAnalyzer = new DirectoryAnalyzer.DirectoryAnalyzer(AppDomain.CurrentDomain.BaseDirectory);

Console.WriteLine("Starting to analyze files in the current directory (common listener attached):");
Console.WriteLine();
directoryAnalyzer.FileFound += CommonFileFoundHandler;
directoryAnalyzer.Start();

Console.WriteLine();
Console.WriteLine("Starting to analyze files in the current directory (common listener detached):");
Console.WriteLine();
directoryAnalyzer.FileFound -= CommonFileFoundHandler;
directoryAnalyzer.Start();

Console.WriteLine();
Console.WriteLine("Starting to analyze files in the current directory (interrupting listener attached):");
Console.WriteLine();
directoryAnalyzer.FileFound += InterruptingFileFoundHandler;
directoryAnalyzer.Start();

Console.WriteLine();
Console.Write("Press any key...");
Console.ReadKey();