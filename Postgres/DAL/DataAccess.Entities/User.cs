﻿using System.Collections.Generic;
using DataAccess.Abstraction;

namespace DataAccess.Entities
{
    public class User: IEntity<long>
    {
        public long Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public virtual ICollection<Lot> Lots { get; set; }

        public bool Deleted { get; set; }
    }
}