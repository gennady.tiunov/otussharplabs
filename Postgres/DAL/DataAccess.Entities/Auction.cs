﻿using System;
using DataAccess.Abstraction;

namespace DataAccess.Entities
{
    public class Auction : IEntity<long>
    {
        public long Id { get; set; }

        public virtual Lot Lot { get; set; }

        public decimal StartingBid { get; set; }
        
        public string Currency { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime FinishTime { get; set; }

        public bool Deleted { get; set; }
    }
}