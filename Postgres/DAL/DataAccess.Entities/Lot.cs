﻿using DataAccess.Abstraction;

namespace DataAccess.Entities
{
    public class Lot : IEntity<long>
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public virtual User Owner { get; set; }

        public bool Deleted { get; set; }
    }
}