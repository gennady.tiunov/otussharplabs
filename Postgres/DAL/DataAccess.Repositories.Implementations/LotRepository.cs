﻿using DataAccess.Entities;
using DataAccess.EntityFramework;
using DataAccess.Implementation;

namespace DataAccess.Repositories
{
    public class LotRepository : Repository<Lot, long>, ILotRepository
    {
        public LotRepository(DatabaseContext context): base(context)
        {
        }
    }
}
