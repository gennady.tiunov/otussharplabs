﻿using DataAccess.Entities;
using DataAccess.EntityFramework;
using DataAccess.Implementation;

namespace DataAccess.Repositories
{
    public class UserRepository: Repository<User, long>, IUserRepository 
    {
        public UserRepository(DatabaseContext context): base(context)
        {
        }
    }
}
