﻿using DataAccess.Entities;
using DataAccess.EntityFramework;
using DataAccess.Implementation;

namespace DataAccess.Repositories
{
    public class AuctionRepository : Repository<Auction, long>, IAuctionRepository
    {
        public AuctionRepository(DatabaseContext context): base(context)
        {
        }
    }
}
