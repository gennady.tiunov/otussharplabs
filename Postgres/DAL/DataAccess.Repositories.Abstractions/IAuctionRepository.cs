﻿using DataAccess.Abstraction;
using DataAccess.Entities;

namespace DataAccess.Repositories
{
    public interface IAuctionRepository : IRepository<Auction, long>
    {
    }
}