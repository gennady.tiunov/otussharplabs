﻿using DataAccess.Abstraction;
using DataAccess.Entities;

namespace DataAccess.Repositories
{
    public interface IUserRepository: IRepository<User, long>
    {
    }
}