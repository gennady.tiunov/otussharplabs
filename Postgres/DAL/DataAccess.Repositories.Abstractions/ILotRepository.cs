﻿using DataAccess.Abstraction;
using DataAccess.Entities;

namespace DataAccess.Repositories
{
    public interface ILotRepository: IRepository<Lot, long>
    {
    }
}