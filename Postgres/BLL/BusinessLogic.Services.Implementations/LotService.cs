﻿using BusinessLogic.Abstractions;
using DataAccess.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogic.Contracts;
using DataAccess.Repositories;

namespace BusinessLogic.Services
{
    public class LotService : ILotService
    {
        private readonly IMapper _mapper;
        private readonly ILotRepository _lotRepository;

        public LotService(
            IMapper mapper,
            ILotRepository lotRepository)
        {
            _mapper = mapper;
            _lotRepository = lotRepository;
        }

        public async Task<ICollection<LotDto>> GetAllAsync()
        {
            ICollection<Lot> entities = await _lotRepository.GetAllAsync();
            return _mapper.Map<ICollection<Lot>, ICollection<LotDto>>(entities);
        }

        public async Task<LotDto> GetByIdAsync(long id)
        {
            var lot = await _lotRepository.GetAsync(id);
            return _mapper.Map<LotDto>(lot);
        }

        public async Task<long> CreateAsync(LotDto lotDto)
        {
            var entity = _mapper.Map<LotDto, Lot>(lotDto);
            var result = await _lotRepository.AddAsync(entity);
            await _lotRepository.SaveChangesAsync();
            return result.Id;
        }

        public async Task UpdateAsync(long id, LotDto lotDto)
        {
            var entity = _mapper.Map<LotDto, Lot>(lotDto);
            entity.Id = id;
            _lotRepository.Update(entity);
            await _lotRepository.SaveChangesAsync();
        }

        public async Task DeleteAsync(long id)
        {
            var lot = await _lotRepository.GetAsync(id);
            lot.Deleted = true; 
            await _lotRepository.SaveChangesAsync();
        }
    }
}