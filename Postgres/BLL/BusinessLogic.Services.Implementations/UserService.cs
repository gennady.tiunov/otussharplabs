﻿using BusinessLogic.Abstractions;
using DataAccess.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogic.Contracts;
using DataAccess.Repositories;

namespace BusinessLogic.Services
{
    public class UserService : IUserService
    {
        private readonly IMapper _mapper;
        private readonly IUserRepository _userRepository;

        public UserService(
            IMapper mapper,
            IUserRepository lotRepository)
        {
            _mapper = mapper;
            _userRepository = lotRepository;
        }

        public async Task<ICollection<UserDto>> GetAllAsync()
        {
            ICollection<User> entities = await _userRepository.GetAllAsync();
            return _mapper.Map<ICollection<User>, ICollection<UserDto>>(entities);
        }

        public async Task<UserDto> GetByIdAsync(long id)
        {
            var user = await _userRepository.GetAsync(id);
            return _mapper.Map<UserDto>(user);
        }

        public async Task<long> CreateAsync(UserDto userDto)
        {
            var entity = _mapper.Map<UserDto, User>(userDto);
            var result = await _userRepository.AddAsync(entity);
            await _userRepository.SaveChangesAsync();
            return result.Id;
        }

        public async Task UpdateAsync(long id, UserDto userDto)
        {
            var entity = _mapper.Map<UserDto, User>(userDto);
            entity.Id = id;
            _userRepository.Update(entity);
            await _userRepository.SaveChangesAsync();
        }

        public async Task DeleteAsync(long id)
        {
            var user = await _userRepository.GetAsync(id);
            user.Deleted = true; 
            await _userRepository.SaveChangesAsync();
        }
    }
}