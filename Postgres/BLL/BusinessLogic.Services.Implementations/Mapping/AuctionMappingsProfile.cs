using AutoMapper;
using BusinessLogic.Contracts;
using DataAccess.Entities;

namespace BusinessLogic.Services.Mapping
{
    public class AuctionMappingsProfile : Profile
    {
        public AuctionMappingsProfile()
        {
            CreateMap<Auction, AuctionDto>();
            
            CreateMap<AuctionDto, Auction>()
                .ForMember(d => d.Id, map => map.Ignore())
                .ForMember(d => d.Deleted, map => map.Ignore());
        }
    }
}