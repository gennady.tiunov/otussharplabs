using AutoMapper;
using BusinessLogic.Contracts;
using DataAccess.Entities;

namespace BusinessLogic.Services.Mapping
{
    public class LotMappingsProfile : Profile
    {
        public LotMappingsProfile()
        {
            CreateMap<Lot, LotDto>();
            
            CreateMap<LotDto, Lot>()
                .ForMember(d => d.Id, map => map.Ignore())
                .ForMember(d => d.Deleted, map => map.Ignore());
        }
    }
}