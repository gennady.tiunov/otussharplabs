﻿using BusinessLogic.Abstractions;
using DataAccess.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogic.Contracts;
using DataAccess.Repositories;

namespace BusinessLogic.Services
{
    public class AuctionService : IAuctionService
    {
        private readonly IMapper _mapper;
        private readonly IAuctionRepository _auctionRepository;

        public AuctionService(
            IMapper mapper,
            IAuctionRepository auctionRepositoryy)
        {
            _mapper = mapper;
            _auctionRepository = auctionRepositoryy;
        }

        public async Task<ICollection<AuctionDto>> GetAllAsync()
        {
            ICollection<Auction> entities = await _auctionRepository.GetAllAsync();
            return _mapper.Map<ICollection<Auction>, ICollection<AuctionDto>>(entities);
        }

        public async Task<AuctionDto> GetByIdAsync(long id)
        {
            var auction = await _auctionRepository.GetAsync(id);
            return _mapper.Map<AuctionDto>(auction);
        }

        public async Task<long> CreateAsync(AuctionDto auctionDto)
        {
            var entity = _mapper.Map<AuctionDto, Auction>(auctionDto);
            var result = await _auctionRepository.AddAsync(entity);
            await _auctionRepository.SaveChangesAsync();
            return result.Id;
        }

        public async Task UpdateAsync(long id, AuctionDto auctionDto)
        {
            var entity = _mapper.Map<AuctionDto, Auction>(auctionDto);
            entity.Id = id;
            _auctionRepository.Update(entity);
            await _auctionRepository.SaveChangesAsync();
        }

        public async Task DeleteAsync(long id)
        {
            var auction = await _auctionRepository.GetAsync(id);
            auction.Deleted = true; 
            await _auctionRepository.SaveChangesAsync();
        }
    }
}