﻿using System.Collections.Generic;

namespace BusinessLogic.Contracts
{
    public class UserDto
    {
        public long Id { get; set; }
        
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public virtual ICollection<LotDto> Lots { get; set; }
    }
}