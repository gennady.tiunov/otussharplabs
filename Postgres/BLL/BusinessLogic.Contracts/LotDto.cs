﻿namespace BusinessLogic.Contracts
{
    public class LotDto
    {
        public long Id { get; set; }
        
        public string Name { get; set; }

        public string Description { get; set; }

        public UserDto Owner { get; set; }
    }
}