﻿using System;

namespace BusinessLogic.Contracts
{
    public class AuctionDto
    {
        public long Id { get; set; }

        public virtual LotDto Lot { get; set; }

        public decimal StartingBid { get; set; }

        public string Currency { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime FinishTime { get; set; }
    }
}