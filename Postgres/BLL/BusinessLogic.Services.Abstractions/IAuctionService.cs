using BusinessLogic.Contracts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BusinessLogic.Abstractions
{
    public interface IAuctionService
    {
        Task<ICollection<AuctionDto>> GetAllAsync();

        Task<AuctionDto> GetByIdAsync(long id);

        Task<long> CreateAsync(AuctionDto auctionDto);

        Task UpdateAsync(long id, AuctionDto auctionDto);

        Task DeleteAsync(long id);
    }
}