using BusinessLogic.Contracts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BusinessLogic.Abstractions
{
    public interface IUserService
    {
        Task<ICollection<UserDto>> GetAllAsync();

        Task<UserDto> GetByIdAsync(long id);

        Task<long> CreateAsync(UserDto userDto);

        Task UpdateAsync(long id, UserDto userDto);

        Task DeleteAsync(long id);
    }
}