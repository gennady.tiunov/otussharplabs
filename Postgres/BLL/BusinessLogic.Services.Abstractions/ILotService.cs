using BusinessLogic.Contracts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BusinessLogic.Abstractions
{
    public interface ILotService
    {
        Task<ICollection<LotDto>> GetAllAsync();

        Task<LotDto> GetByIdAsync(long id);

        Task<long> CreateAsync(LotDto lotDto);

        Task UpdateAsync(long id, LotDto lotDto);

        Task DeleteAsync(long id);
    }
}