﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogic.Abstractions;
using BusinessLogic.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApi.Models;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController: ControllerBase
    {
        private readonly IUserService _userService;
        private readonly ILogger<UsersController> _logger;
        private readonly IMapper _mapper;

        public UsersController(
	        IUserService userService,
	        ILogger<UsersController> logger,
	        IMapper mapper)
        {
            _userService = userService;
            _logger = logger;
            _mapper = mapper;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAll()
        {
	        return Ok(_mapper.Map<List<UserModel>>(await _userService.GetAllAsync()));
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> Get(long id)
        {
	        var userDto = await _userService.GetByIdAsync(id);
	        if (userDto == null)
	        {
		        return NotFound();
	        }

            return Ok(_mapper.Map<UserModel>(userDto));
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> Add(UserModel userDto)
        {
            var userId = await _userService.CreateAsync(_mapper.Map<UserDto>(userDto));
            var userUrl = Url.Action("Get", new { id = userId });

            return Created(userUrl, userId);
        }
        
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> Edit(long id, UserModel userDto)
        {
	        var existingUser = await _userService.GetByIdAsync(id);
	        if (existingUser == null)
	        {
		        return NotFound();
	        }

            await _userService.UpdateAsync(id, _mapper.Map<UserDto>(userDto));

            return Ok();
        }
        
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> Delete(long id)
        {
	        var existingUser = await _userService.GetByIdAsync(id);
	        if (existingUser == null)
	        {
		        return NotFound();
	        }

            await _userService.DeleteAsync(id);

            return NoContent();
        }
    }
}