using AutoMapper;
using BusinessLogic.Contracts;
using WebApi.Models;

namespace WebApi.Mapping
{
    public class UserMappingsProfile : Profile
    {
        public UserMappingsProfile()
        {
            CreateMap<UserDto, UserModel>().ReverseMap();
        }
    }
}
