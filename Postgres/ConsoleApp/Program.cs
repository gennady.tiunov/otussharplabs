﻿using System.Text.Json;
using BusinessLogic.Abstractions;
using BusinessLogic.Contracts;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace ConsoleApp
{
	public class Program
	{
		private readonly IUserService _userService;
		private readonly ILotService _lotService;
		private readonly IAuctionService _auctionService;
		
		static void Main(string[] args)
		{
			var host = CreateHostBuilder(args).Build();

			host.Services.GetRequiredService<Program>().Run();
		}

		public Program(
			IUserService userService,
			ILotService lotService,
			IAuctionService auctionService)
		{
			_userService = userService;
			_lotService = lotService;
			_auctionService = auctionService;
		}

		public void Run()
		{
			while (true)
			{
				Console.Clear();

				Console.WriteLine("Press the key:");
				Console.WriteLine("Esc - to exit");
				Console.WriteLine("S - to show DB content");
				Console.WriteLine("U - to add a new user");

				switch (Console.ReadKey().Key)
				{
					case ConsoleKey.Escape:

						Environment.Exit(0);

						break;

					case ConsoleKey.S:

						Console.Clear();
						ShowDbContent();
						Thread.Sleep(7000);

						break;

					case ConsoleKey.U:

						Console.WriteLine();
						Console.WriteLine("Enter a new user as JSON ({ \"FirstName\" : \"Value\", \"LastName\" : \"Value\" }):");
						
						try
						{
							var userJson = Console.ReadLine();
							var userDto = JsonSerializer.Deserialize<UserDto>(userJson);
							_userService.CreateAsync(userDto).Wait();

							Console.Clear();
							ShowDbContent();
							Thread.Sleep(7000);
						}
						catch (Exception e)
						{
							
							Console.WriteLine($"Error deserializing user from JSON entered: {e.Message}");
							Thread.Sleep(5000);
						}
						
						break;

					default:
						Console.Clear();
						Console.WriteLine("Unknown key!");
						Thread.Sleep(5000);

						break;
				}
			}
		}

		private static IHostBuilder CreateHostBuilder(string[] args) =>
			Host.CreateDefaultBuilder(args)
				.ConfigureServices(services =>
				{
					var builder = new ConfigurationBuilder()
						.SetBasePath(Directory.GetCurrentDirectory())
						.AddJsonFile("appsettings.json", optional: false, reloadOnChange: false);

					IConfiguration configuration = builder.Build();

					new Startup(configuration).ConfigureServices(services);
				});

		private void ShowDbContent()
		{
			var users = _userService.GetAllAsync().Result;
			foreach (var user in users)
			{
				Console.WriteLine($"User: Id = '{user.Id}', FirstName = '{user.FirstName}', LastName = '{user.LastName}', Lot Count = {user.Lots.Count}");
			}

			Console.WriteLine();

			var lots = _lotService.GetAllAsync().Result;
			foreach (var lot in lots)
			{
				Console.WriteLine($"Lot: Id = '{lot.Id}', Name = '{lot.Name}', Description = '{lot.Description}', Owner = '{lot.Owner.FirstName} {lot.Owner.LastName}'");
			}

			Console.WriteLine();

			var auctions = _auctionService.GetAllAsync().Result;
			foreach (var auction in auctions)
			{
				Console.WriteLine($"Auction: Id = '{auction.Id}', Lot = '{auction.Lot.Name}', Starting Bid = '{auction.Currency} {auction.StartingBid}', Start Time = '{auction.StartTime}', Finish Time = '{auction.FinishTime}'");
			}
		}
	}
}
