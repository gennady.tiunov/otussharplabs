CREATE DATABASE "Auction"
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'Russian_Russia.1251'
    LC_CTYPE = 'Russian_Russia.1251'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

CREATE TABLE IF NOT EXISTS public."Users"
(
    "Id" bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1 ),
    "FirstName" character varying(50) COLLATE pg_catalog."default" NOT NULL,
    "LastName" character varying(50) COLLATE pg_catalog."default" NOT NULL,
	"Deleted" boolean NOT NULL,
    CONSTRAINT "Users_pkey" PRIMARY KEY ("Id")
)

TABLESPACE pg_default;

ALTER TABLE public."Users"
    OWNER to postgres;

CREATE TABLE IF NOT EXISTS public."Lots"
(
    "Id" bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1 ),
    "Name" character varying(50) COLLATE pg_catalog."default" NOT NULL,
    "Description" text COLLATE pg_catalog."default" NOT NULL,
    "OwnerId" bigint NOT NULL,
	"Deleted" boolean NOT NULL,
    CONSTRAINT "Lots_pkey" PRIMARY KEY ("Id"),
    CONSTRAINT "FK_Lots_OwnerId_Users_Id" FOREIGN KEY ("OwnerId")
        REFERENCES public."Users" ("Id") MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public."Lots"
    OWNER to postgres;

CREATE TABLE IF NOT EXISTS public."Auctions"
(
    "Id" bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1 ),
    "LotId" bigint NOT NULL,
    "StartingBid" numeric(10, 2) NOT NULL,
    "Currency" character varying(5) COLLATE pg_catalog."default" NOT NULL,
    "StartTime" timestamp with time zone NOT NULL,
    "FinishTime" timestamp with time zone NOT NULL,
	"Deleted" boolean NOT NULL,
    CONSTRAINT "Auctions_pkey" PRIMARY KEY ("Id"),
    CONSTRAINT "FK_Auctions_LotId_Lots_Id" FOREIGN KEY ("LotId")
        REFERENCES public."Lots" ("Id") MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public."Auctions"
    OWNER to postgres;