DO $$

DECLARE
	UserId bigint;
	LotId bigint;
		
BEGIN
	INSERT INTO "Users" ("FirstName", "LastName", "Deleted")
		VALUES ('Konstantin', 'Konstantinov', 'FALSE');

	INSERT INTO "Users" ("FirstName", "LastName", "Deleted")
		VALUES ('Oleg', 'Olegov', 'FALSE');

	INSERT INTO "Users" ("FirstName", "LastName", "Deleted")
		VALUES ('Ivan', 'Ivanov', 'FALSE')
  			RETURNING "Id" INTO UserId;
		
	INSERT INTO "Lots" ("Name", "Description", "OwnerId", "Deleted")
		VALUES ('War and Peace', 'The most famous novel by Leo Tolstoy.', UserId, 'FALSE')
  			RETURNING "Id" INTO LotId;
		
	INSERT INTO "Auctions" ("LotId", "StartingBid", "Currency", "StartTime", "FinishTime", "Deleted")
		VALUES (LotId, 14.99, 'EUR', '2021-11-01T00:00:00.000Z', '2021-11-01T23:59:59.999Z', 'FALSE')
  			RETURNING "Id" INTO LotId;
			
	INSERT INTO "Lots" ("Name", "Description", "OwnerId", "Deleted") VALUES ('Anna Karenina', 'The dramatic story of love between Anna Karenina and Alexey Vronsky.', UserId, 'FALSE')
  		RETURNING "Id" INTO LotId;
		
	INSERT INTO "Auctions" ("LotId", "StartingBid", "Currency", "StartTime", "FinishTime", "Deleted")
		VALUES (LotId, 9.99, 'EUR', '2021-11-02T00:00:00.000Z', '2021-11-02T23:59:59.999Z', 'FALSE')
  			RETURNING "Id" INTO LotId;

	INSERT INTO "Users" ("FirstName", "LastName", "Deleted")
		VALUES ('Petr', 'Petrov', 'FALSE')
  			RETURNING "Id" INTO UserId;

	INSERT INTO "Lots" ("Name", "Description", "OwnerId", "Deleted")
		VALUES ('Audi R8 ', 'Luxury and sport.', UserId, 'FALSE')
  			RETURNING "Id" INTO LotId;
		
	INSERT INTO "Auctions" ("LotId", "StartingBid", "Currency", "StartTime", "FinishTime", "Deleted")
		VALUES (LotId, 250000.00, 'USD', '2021-12-01T00:00:00.000Z', '2021-12-01T23:59:59.999Z', 'FALSE')
  			RETURNING "Id" INTO LotId;
			
	INSERT INTO "Users" ("FirstName", "LastName", "Deleted")
		VALUES ('Sidor', 'Sidorov', 'FALSE')
  			RETURNING "Id" INTO UserId;

	INSERT INTO "Lots" ("Name", "Description", "OwnerId", "Deleted")
		VALUES ('2-week tour to Altai', 'Breathtaking journey to pristine locations in Altai mountains.', UserId, 'FALSE')
  			RETURNING "Id" INTO LotId;
		
	INSERT INTO "Auctions" ("LotId", "StartingBid", "Currency", "StartTime", "FinishTime", "Deleted")
		VALUES (LotId, 150000.00, 'RUB', '2021-12-03T00:00:00.000Z', '2021-12-03T23:59:59.999Z', 'FALSE')
  			RETURNING "Id" INTO LotId;

	INSERT INTO "Lots" ("Name", "Description", "OwnerId", "Deleted")
		VALUES ('Cuba Libre', '2-week packed tour to the Island of Freedom.', UserId, 'FALSE')
  			RETURNING "Id" INTO LotId;
		
	INSERT INTO "Auctions" ("LotId", "StartingBid", "Currency", "StartTime", "FinishTime", "Deleted")
		VALUES (LotId, 150000.00, 'RUB', '2021-12-03T00:00:00.000Z', '2021-12-03T23:59:59.999Z', 'FALSE')
  			RETURNING "Id" INTO LotId;
END $$;