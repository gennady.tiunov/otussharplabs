﻿using BusinessLogic.Abstractions;
using BusinessLogic.Services;
using ComponentRegistrar.Settings;
using DataAccess.EntityFramework;
using DataAccess.Repositories;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ComponentRegistrar
{
    public static class Registrar
    {
        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            var applicationSettings = configuration.Get<ApplicationSettings>();

            services.AddSingleton(applicationSettings);

            return services
	            .AddSingleton((IConfigurationRoot)configuration)
                .InstallServices()
                .ConfigureContext(applicationSettings.ConnectionString)
                .InstallRepositories();
        }
        
        private static IServiceCollection InstallServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IUserService, UserService>();
            serviceCollection.AddTransient<ILotService, LotService>();
            serviceCollection.AddTransient<IAuctionService, AuctionService>();

            return serviceCollection;
        }
        
        private static IServiceCollection InstallRepositories(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IUserRepository, UserRepository>();
            serviceCollection.AddTransient<ILotRepository, LotRepository>();
            serviceCollection.AddTransient<IAuctionRepository, AuctionRepository>();

            return serviceCollection;
        }
    }
}