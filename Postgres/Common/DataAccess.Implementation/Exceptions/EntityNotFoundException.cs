﻿using System;

namespace DataAccess.Implementation.Exceptions
{
    public class EntityNotFoundException<T, TPrimaryKey> : Exception
    {
        public EntityNotFoundException(TPrimaryKey id) : base($"Entity '{typeof(T).Name}' with primary key '{id}' not found.") { }
    }
}