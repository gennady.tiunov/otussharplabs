﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DataAccess.Abstraction;
using DataAccess.Implementation.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Implementation
{
    public abstract class Repository<T, TPrimaryKey> : ReadRepository<T, TPrimaryKey>, IRepository<T, TPrimaryKey>
        where T : class, IEntity<TPrimaryKey>
    {
        protected Repository(DbContext context): base(context)
        {
        }

        public virtual T Add(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            var objToReturn = Context.Set<T>().Add(entity);
            return objToReturn.Entity;
        }

        public virtual async Task<T> AddAsync(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            return (await Context.Set<T>().AddAsync(entity)).Entity;
        }

        public virtual void AddRange(List<T> entities)
        {
            if (entities == null || !entities.Any())
            {
                throw new ArgumentNullException(nameof(entities));
            }

            var enumerable = entities as IList<T> ?? entities.ToList();
            Context.Set<T>().AddRange(enumerable);
        }

        public virtual async Task AddRangeAsync(ICollection<T> entities)
        {
            if (entities == null || !entities.Any())
            {
                throw new ArgumentNullException(nameof(entities));
            }

            await EntitySet.AddRangeAsync(entities);
        }
        
        public virtual void Update(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            Context.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Delete(TPrimaryKey id)
        {
	        var entity = EntitySet.Find(id);
	        if (entity == null)
	        {
                throw new EntityNotFoundException<T, TPrimaryKey>(id);
            }
	        EntitySet.Remove(entity);
        }

        public virtual void Delete(T entity)
        {
	        if (entity == null)
	        {
                throw new ArgumentNullException(nameof(entity));
            }
            Context.Entry(entity).State = EntityState.Deleted;
        }

        public virtual void DeleteRange(ICollection<T> entities)
        {
	        if (entities == null || !entities.Any())
	        {
                throw new ArgumentNullException(nameof(entities));
            }
	        EntitySet.RemoveRange(entities);
        }

        public virtual void SaveChanges()
        {
            Context.SaveChanges();
        }

        public virtual async Task SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            await Context.SaveChangesAsync(cancellationToken);
        }
    }
}