using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DataAccess.Abstraction
{
    public interface IReadRepository<T, TPrimaryKey> : IRepository where T : IEntity<TPrimaryKey>
    {
        IQueryable<T> GetAll(bool noTracking = false);

        Task<List<T>> GetAllAsync(CancellationToken cancellationToken = default, bool asNoTracking = false);

        T Get(TPrimaryKey id);

        Task<T> GetAsync(TPrimaryKey id);
    }
}