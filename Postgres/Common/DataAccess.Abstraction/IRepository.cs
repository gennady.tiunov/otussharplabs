using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace DataAccess.Abstraction
{
    public interface IRepository
    {
    }

    public interface IRepository<T, TPrimaryKey> : IReadRepository<T, TPrimaryKey>
        where T : IEntity<TPrimaryKey>
    {
        T Add(T entity);

        Task<T> AddAsync(T entity);

        void AddRange(List<T> entities);

        Task AddRangeAsync(ICollection<T> entities);

        void Update(T entity);

        void Delete(TPrimaryKey id);

        void Delete(T entity);

        void DeleteRange(ICollection<T> entities);

        void SaveChanges();

        Task SaveChangesAsync(CancellationToken cancellationToken = default);
    }
}