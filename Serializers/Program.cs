﻿using Newtonsoft.Json;
using Serializers.Entities;
using Serializers.Serialization;

const int IterationCount = 1_000_000;

string? serializedEntity = null;

var f = F.Get();

var fSerializer = new ReflectionBasedCsvEntitySerializer<F>();
Console.WriteLine($"Reflection based serialization started for entity '{typeof(F)}' ({IterationCount} iterations)");
var startTime = DateTime.Now;
for (var i = 0; i < IterationCount; i++) 
{
    serializedEntity = fSerializer.Serialize(f);
}
File.WriteAllText($"{nameof(F)}.csv", serializedEntity);
Console.WriteLine($"Reflection based serialization finished for entity '{typeof(F)}' (duration: {(DateTime.Now - startTime).TotalSeconds} seconds)");
Console.WriteLine("CSV:");
Console.WriteLine(serializedEntity);

Console.WriteLine();

Console.WriteLine($"JSON.NET serialization started for entity '{typeof(F)}' ({IterationCount}) iterations");
startTime = DateTime.Now;
for (var i = 0; i < IterationCount; i++)
{
    serializedEntity = JsonConvert.SerializeObject(f);
}
File.WriteAllText($"{nameof(F)}.json", serializedEntity);
Console.WriteLine($"JSON.NET serialization finished for entity '{typeof(F)}' (duration: {(DateTime.Now - startTime).TotalSeconds} seconds)");
Console.WriteLine("JSON:");
Console.WriteLine(serializedEntity);

Console.WriteLine();

var userSerializer = new ReflectionBasedCsvEntitySerializer<User>();
Console.WriteLine($"Reflection based serialization started for entity '{typeof(User)}'");
var user = new User
{
    Id = Guid.NewGuid(),
    FirstName = "Ivan",
    BirthDate = DateTime.Now.AddYears(-30),
    Role = Role.Customer,
    _nickname = "i\"va\",\"shka"
};
user.ChangePrivateMembers();
serializedEntity = userSerializer.Serialize(user);
File.WriteAllText($"{nameof(User)}.csv", serializedEntity);
Console.WriteLine($"Reflection based serialization finished for entity '{typeof(User)}'");
Console.WriteLine("CSV:");
Console.WriteLine(serializedEntity);

Console.WriteLine();
Console.WriteLine("Press any key to deserialize entities from files");
Console.ReadKey();
Console.WriteLine();

serializedEntity = File.ReadAllText($"{nameof(F)}.csv");
Console.WriteLine($"Reflection based deserialization started for entity '{typeof(F)}' ({IterationCount}) iterations)");
startTime = DateTime.Now;
for (var i = 0; i < IterationCount; i++)
{
    f = fSerializer.Deserialize(serializedEntity);
}
Console.WriteLine($"Reflection based deserialization finished for entity '{typeof(F)}' (duration: {(DateTime.Now - startTime).TotalSeconds} seconds)");

Console.WriteLine();

serializedEntity = File.ReadAllText($"{nameof(F)}.json");
Console.WriteLine($"JSON.NET deserialization started for entity '{typeof(F)}' ({IterationCount}) iterations");
startTime = DateTime.Now;
for (var i = 0; i < IterationCount; i++)
{
    f = JsonConvert.DeserializeObject<F>(serializedEntity);
}
Console.WriteLine($"JSON.NET deserialization finished for entity '{typeof(F)}' (duration: {(DateTime.Now - startTime).TotalSeconds} seconds)");

Console.WriteLine();

serializedEntity = File.ReadAllText($"{nameof(User)}.csv");
Console.WriteLine($"Reflection based deserialization started for entity '{typeof(User)}'");
user = userSerializer.Deserialize(serializedEntity);
Console.WriteLine($"Reflection based deserialization finished for entity '{typeof(User)}'");

Console.WriteLine();
Console.WriteLine("Press any key to exit");
Console.ReadKey(); 