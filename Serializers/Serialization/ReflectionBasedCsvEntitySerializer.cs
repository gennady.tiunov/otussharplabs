﻿using System.ComponentModel;
using System.Reflection;
using System.Text;

namespace Serializers.Serialization
{
    public class ReflectionBasedCsvEntitySerializer<T> : IEntitySelializer<T>
        where T : class, new()
    {
        public string Serialize(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            var entityType = entity.GetType();

            var members = entityType
             .GetMembers(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
             .Where(m => m.MemberType == MemberTypes.Property || m.MemberType == MemberTypes.Field)
             .ToArray();

            if (members.Length == 0)
            {
                throw new SerializationException($"Could not serialize entity '{entityType.Name}' as it has neither properties nor fields");
            }

            var csv = new StringBuilder();

            for (var i = 0; i < members.Length; i++)
            {
                var memberName = members[i].Name;
                if (!memberName.Contains("k__BackingField"))
                {
                    csv.Append(memberName);
                    csv.Append(Constants.CsvDelimiter);
                }
            }

            csv.Replace(Constants.CsvDelimiter, Environment.NewLine, csv.Length - 1, 1);

            for (var i = 0; i < members.Length; i++)
            {
                var member = members[i];
                if (member.Name.Contains("k__BackingField"))
                {
                    continue;
                }

                object? value;

                switch (member.MemberType)
                {
                    case MemberTypes.Field:
                        value = ((FieldInfo)member).GetValue(entity);
                        break;
                    case MemberTypes.Property:
                        value = ((PropertyInfo)member).GetValue(entity);
                        break;
                    default:
                        throw new SerializationException($"Member '{member.Name}' is neither property nor field");
                }

                string? stringValue = value == null ? null : value.ToString();

                csv.Append(stringValue.EscapeDelimiter().EscapeQuotes().Quote());
                csv.Append(Constants.CsvDelimiter);
            }

            csv.Remove(csv.Length - 1, 1);

            return csv.ToString();
        }

        public T Deserialize(string serializedEntity)
        {
            (var columnNames, var values) = AssertValidCsvData(serializedEntity);

            var result = new T();

            for (var i = 0; i < columnNames.Length; i++)
            {
                var entityType = typeof(T);

                try
                {
                    var value = values[i]?.Trim('"').NormalizeQuotes().NormalizeDelimiter();

                    var property = entityType.GetProperty(columnNames[i], BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                    if (property != null)
                    {
                        if (value != null)
                        {
                            var propertyType = property.PropertyType;
                            var typeConverter = TypeDescriptor.GetConverter(propertyType);
                            property.SetValue(result, typeConverter.ConvertFrom(value));
                        }
                        continue;
                    }
                    var field = entityType.GetField(columnNames[i], BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                    if (field != null)
                    {
                        if (value != null)
                        {
                            var fieldType = field.FieldType;
                            var typeConverter = TypeDescriptor.GetConverter(fieldType);
                            field.SetValue(result, typeConverter.ConvertFrom(value));
                        }
                        continue;
                    }
                }
                catch (Exception exception)
                {
                    throw new SerializationException($"Error occurred while setting value '{values[i]}' to property / field '{columnNames[i]}' on entity '{entityType.Name}': {exception.Message}");
                }

                throw new SerializationException($"Neither property nor field '{columnNames[i]}' found on entity '{entityType.Name}'");
            }

            return result;
        }

        private (string[] columnNames, string[] values) AssertValidCsvData(string serializedEntity)
        {
            if (string.IsNullOrEmpty(serializedEntity))
            {
                throw new ArgumentNullException(nameof(serializedEntity));
            }

            using var reader = new StringReader(serializedEntity);

            var headerLine = reader.ReadLine();
            if (string.IsNullOrEmpty(headerLine))
            {
                throw new SerializationException("Header line in document is empty");
            }

            var columnNames = headerLine.Split(Constants.CsvDelimiter, StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries);
            if (columnNames.Length == 0)
            {
                throw new SerializationException("Header line has no column names specified");
            }

            var valueLine = reader.ReadLine();
            if (string.IsNullOrEmpty(valueLine))
            {
                throw new SerializationException("Document contains no entities to deserialize");
            }

            var values = valueLine.Split($"\"{Constants.CsvDelimiter}\"", StringSplitOptions.TrimEntries);
            if (values.Length == 0)
            {
                throw new SerializationException("Entity line has no values specified");
            }

            if (columnNames.Length != values.Length)
            {
                throw new SerializationException($"Number of columns ({columnNames.Length}) does not coinside number of entity properties / fields ({values.Length})");
            }

            return (columnNames, values);
        }
    }
}
