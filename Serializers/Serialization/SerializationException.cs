﻿namespace Serializers.Serialization
{
    public class SerializationException : InvalidOperationException
    {
        public SerializationException(string message) : base(message) { }
    }
}
