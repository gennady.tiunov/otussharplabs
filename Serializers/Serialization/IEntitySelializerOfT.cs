﻿namespace Serializers.Serialization
{
    public interface IEntitySelializer<T> where T : class, new()
    {
        string Serialize(T entity);

        T Deserialize(string serializedEntity);
    }
}
