﻿namespace Serializers.Entities
{
    public class User
    {
        public Guid Id { get; set; }

        public string? FirstName { get; set; }

        public string? LastName { get; set; }

        public DateTime BirthDate { get; set; }

        public Role Role { get; set; }

        private bool IsDeleted { get; set; }

        public string? _nickname;

        private int _childrenCount;

        public void ChangePrivateMembers()
        {
            IsDeleted = true;
            _childrenCount = 10;
        }
    }
}
