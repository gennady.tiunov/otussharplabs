﻿namespace Serializers.Entities
{
    public enum Role
    {
        Admin,
        Owner,
        SaleManager,
        CatalogManeger,
        PromoManager,
        Customer
    }
}
